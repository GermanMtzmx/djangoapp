import magic
from django.utils.deconstruct import deconstructible
from django.core.exceptions import ValidationError


@deconstructible
class MimeTypeValidator(object):
	
	def __init__(self,mimetypes):
		self.mimetypes = mimetypes
	
	def __call__(self,mimetypes):

		try:
			mime = magic.from_buffer(value.read(1024),mime=True)

			if not mime in self.mimetypes:
				raise ValidationError('%s is not acceptable file type ' % value)

		except AttributeError as e:
			raise ValidationError("This value could not be validated for filetype %s" % value)
