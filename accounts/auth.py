from accounts.models import User
from django.contrib.auth.hashers import check_password
from common.utils import CommonMixin



def authenticate(email,password):

	user = CommonMixin.getObjectOrNone(User,email=email)
	if user and not check_password(password,user.password):
		return None
	return user
