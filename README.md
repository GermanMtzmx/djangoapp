# Django boilerplate

## Getting started

### Install linux dependencies

```shell
sudo apt-get install libpq-dev python-dev python3-dev postgresql postgresql-contrib python-virtualenv python-pip

```

### Virtualenv configuration

Create a new virtualenv

```shell
virtualenv -p /usr/bin/python3  virtualenvname

```

Activate the virtualenv 

```shell

source /path/to/yourVirtualenv/virtualenvName/bin/activate

```

Install dependencies into virtualenv

under the project folder `webservices-djangotemplate` install requirements with pip (activate the virtualenv before install dependences)

```shell
pip install -r requirements.txt
```

### Collect statics files

collect static files  with `python manage.py collectstatic`

### Set a new environment 

Into folder `webservices_djangotemplate` edit the file `envs.py`


Create a new env class by example : 


Sqlite configuration database

```python

class MyClassEnv(Env):
	
	DATABASES = {

		'default': {
			'ENGINE':'django.db.backends.sqlite3',
			'NAME': os.path.join(BASE_DIR,'db.sqlite3'),
		},

	}


```

Postgresql configuration database

```python
class MyClassEnv(Env):
	
	DATABASES = {
		'default': {
			'ENGINE':'django.db.backends.postgresql_psycopg2',
			'NAME':'dbname',
			'USER':'dbuser',
			'PASSWORD':'dbpassword',
			'HOST':'localhost',
			'PORT':'5432'
		},
	}

```


Then add your env class into `envs` dict

```python
	
	envs = {
		'yourHostname':MyClassEnv,
	}

```

### Set custom settings for your env class

MEDIA DOMAIN - To serve correctly media set your custom media domain in your env class by example

```python
MEDIA_DOMAIN = "http://59a65efd.ngrok.io/"

```


EMAIL SETTINGS - Overwrite  email settings in your env class


```python

	EMAIL_HOST = 'YourSmtpHost.com'
	EMAIL_PORT = 587
	EMAIL_USE_TLS = True
	EMAIL_HOST_USER = 'rouruser@mail.com'
	EMAIL_HOST_PASSWORD = 'usserPassword'


```


### Apply migrations

```shell
python manage.py migrate

```

### Create a super user for django admin

```shell
python manage.py createsuperuser

```

#### Run the project

Run  it with testing server

```shell
python manage.py runserver 0.0.0.0:(port)

```


Run it with gunicorn 

```shell

gunicorn --workers (num of workers) --bind 0.0.0.0:(port) --reload --daemon webservices_djangotemplate.wsgi

```


## Django basics

### Create a new app 


* To create a new app must name it in plural
* Must have a url file settings
* Must have a serializer file settings
* Models must name it in singular
* View classes must be descriptive
* Include your app in `webservices_djangotemplate/settings.py`
* Must include url settings into `webservices_djangotemplate/urls.py`



Create new app 


```shell

python manage.py startapp yourAppNameInPlural

```

Create inside the new app folder `urls.py` and `serializers.py`



View content

A simple view could look like this in the `views.py` file of your app

```python

from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from accounts.auth import authenticate


class ExampleAPIView(APIView):
	
	def get(self,request):

		#logic here

		return Response(data=response_data,status=status.HTTP_200_OK)


```

If need views without authentication add 

```python
from accounts.permissions import AllowAny

```

And then in your view class add a variable class named `permission_classes`


```python
	class ExampleAPIView(APIView):

		permission_classes = (AllowAny,)

		def get(self,request):

			#logic here ..
			pass
```



get,post,put,delete,patch methods must return a response always 

See aviable status response [here ...](http://www.django-rest-framework.org/api-guide/status-codes/)

Url content



```python
from django.conf.urls import url 

from yourNewApp.views import (
	ViewExample1,
)

urlpatterns = [

	url(r'^patternOfYourUrl$',viewExample.as_view()),
]
```


Serializer content


```python

from rest_framework import serializers
from yourNewApp.models import (
	YourModel,
)


class ExampleSerializer(serializers.ModelSerializer):

	class Meta:
		model = YourModel
		fields = '__all__'

```

in the Meta class  fields could be a tuple , or instead fields could be a tupe named exclude wich contains
a list of fields of the current model to be excluded


Add urls app to main url settings in `webservices_djangotemplate/urls.py` and your app into `webservices_djangotemplate/settings.py`


Including app into settings


```python

	INSTALLED_APPS = [
		'nameOfYourApp'
	]

```


Including url app setings

```python
	
urlpatterns = [
    url(r'^admin/', admin.site.urls),
    #serve static files
    url(r'^static/(?P<path>.*)$',serve,{'document_root': STATIC_ROOT}),
    #serve media files
    url(r'^api/v1/serve/(?P<path>.*)$',serve,{'document_root':MEDIA_ROOT})

    #apps urls
    url(r'^api/v1/',include('accounts.urls')),
    url(r'^api/v1/',include('common.urls')),
    url(r'^api/v1/',include('yourAppName.urls')),

]


```

Remember always make migration after add a modification in your models with


```shell

python manage.py makemigrations
python manage.py migrate

```


## Django api translations 



To translate response messages into a webservice or into your models

Do the next steps 


* Create messages files into locale dir with 


```python
python manage.py makemessages (languagecode)
```

Django will create the language dir into locale directory  by example ``locale->es->LC_MESSAGES`

inside each language dir will find a file named `django.mo`


Then inside of your views or models  import `ugettext_lazy` lib to translate strings

```python
from django.utils.translation import ugettext_lazy as _
```


If you want translate some response message or string 

```python
	
	response_data  = {}
	response_data["message"] = "Invalid credentials "

```

Change it to 

```python
	
	response_data  = {}
	response_data["message"] = _("Invalid credentials")

```

And everything that you want to translate must be inside `ugettext_lazy` function alias : `_()`


Then to retrive all desired strings to translate run :

```python

python manage.py makemessages

```

And in `django.po` add the translation for every string, once collected django.po file will look like this


```
#: accounts/views.py:94
msgid "Invalid user  or wrong credentials"
msgstr ""
```


Add the translations 

```
#: accounts/views.py:94
msgid "Invalid user  or wrong credentials"
msgstr "Usuario invalido o credenciales erroneas"
```


Once added all translations into `django.po` file of your desired language compile it 

```python 
python manage.py compilemessages
```

To verify add the header `Accept-Language` with the value `es` and will get all response messages translated to spanish



## Adding cronjobs


First create a method inside `common/cron.py`

Then add it in `CRONJOBS` in the `webservices_djangotemplate/settings.py` file

Format used are `('format cron execution','common.cron.YourMethod')`

To Add and run cronjobs :

```shell
	
	python manage.py crontab add

```


To see active cronjobs :

```shell
	
	python manage.py crontab show

```

To remove and stop cronjobs :

```shell
	
	python manage.py crontab remove

```

If you have doubts about cron format : [Cron format online](http://crontab-generator.org/)

If you have doubts about django_cron app : [Crontab django module](https://github.com/kraiz/django-crontab)

