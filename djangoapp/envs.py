import os
import socket


BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

class Env:

	DEBUG = True
	LANGUAGE_CODE = 'es'
	TIME_ZONE = 'UTC'
	USE_I18N = True
	USE_L10N = True
	USE_TZ =  True

	STATIC_URL = '/static/'
	STATIC_ROOT = 'collect_static'
	MEDIA_ROOT = os.path.join(BASE_DIR,'storage')

	#Email configuration

	EMAIL_HOST = 'smtp.gmail.com'
	EMAIL_PORT = 587
	EMAIL_USE_TLS = True
	EMAIL_HOST_USER = 'gms.iorganizacional@gmail.com'
	EMAIL_HOST_PASSWORD = 'p455w0rd'
	#The domain must have /api/v1/serve/ to serve correctly media
	MEDIA_DOMAIN = "http://localhost:8000/"
	FRONTEND_PASSWORD_RESET = 'http://localhost:9001/accounts/resetpassword?token='

	CORS_ORIGIN_ALLOW_ALL = True
	ALLOWED_HOSTS = ['*']
	CORS_ORIGIN_WHITELIST = ()
	CORS_ALLOW_METHODS = (

		'DELETE',
		'POST',
		'PUT',
		'PATCH',
		'OPTIONS',

	)

	CORS_ALLOW_HEADERS = (
		'accept',
		'accept-encoding',
		'authorization',
		'content-type',
		'dnt',
		'origin',
		'user-agent',
		'x-csrftoken',
		'x-requested-with',
		'Time-Zone',
		'HTTP_TIME_ZONE',
		'Accept-Language',

	)

	DATABASES = {

		'default': {
			'ENGINE':'django.db.backends.sqlite3',
			'NAME': os.path.join(BASE_DIR,'db.sqlite3'),
		},

	}



class EnigmaEnv(Env):
	
	DATABASES = {

		'default': {
			'ENGINE':'django.db.backends.sqlite3',
			'NAME': os.path.join(BASE_DIR,'db.sqlite3'),
		},

	}	
		
envs = {
	'enigma':EnigmaEnv,
}


env = envs.get(socket.gethostname())

if not env:
	env = Env