import os
from .envs import env

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.10/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '^7$rcaf(f8-@8-awmt_1-dt0#alpjo$*%pwpuf9023#Fgbe^uhb'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = env.DEBUG

ALLOWED_HOSTS = env.ALLOWED_HOSTS
CORS_ORIGIN_ALLOW_ALL = env.CORS_ORIGIN_ALLOW_ALL


# Application definition

INSTALLED_APPS = [
	'django.contrib.admin',
	'django.contrib.auth',
	'django.contrib.contenttypes',
	'django.contrib.sessions',
	'django.contrib.messages',
	'django.contrib.staticfiles',

	'rest_framework',
    'corsheaders',
    'django_cleanup',
    'django_crontab',
    

	# Apps

	'accounts',
	'common',
]



MIDDLEWARE = [
	'django.middleware.security.SecurityMiddleware',
	'django.contrib.sessions.middleware.SessionMiddleware',
	'django.middleware.locale.LocaleMiddleware',
	'corsheaders.middleware.CorsMiddleware',
	'django.middleware.common.CommonMiddleware',
	
	'django.contrib.auth.middleware.AuthenticationMiddleware',
	'django.contrib.messages.middleware.MessageMiddleware',
	'django.middleware.clickjacking.XFrameOptionsMiddleware',
	#timezone middleware
	'common.middlewares.ActivateTimezoneMiddleware',
	'common.middlewares.AdminTranslateMiddleware',
]

REST_FRAMEWORK = {

	'DEFAULT_AUTHENTICATION_CLASSES': (
		'accounts.authentication.TokenAuthentication',
	),

	'DEFAULT_PERMISSION_CLASSES':(
		'accounts.permissions.IsAuthenticated',
	)
}


ROOT_URLCONF = 'djangoapp.urls'

TEMPLATES = [
	{
		'BACKEND': 'django.template.backends.django.DjangoTemplates',
		'DIRS': [],
		'APP_DIRS': True,
		'OPTIONS': {
			'context_processors': [
				'django.template.context_processors.i18n',
				'django.template.context_processors.debug',
				'django.template.context_processors.request',
				'django.contrib.auth.context_processors.auth',
				'django.contrib.messages.context_processors.messages',
			],
		},
	},
]

WSGI_APPLICATION = 'djangoapp.wsgi.application'


# Database
# https://docs.djangoproject.com/en/1.10/ref/settings/#databases

DATABASES = env.DATABASES

# Password validation
# https://docs.djangoproject.com/en/1.10/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
	{
		'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
	},
	{
		'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
	},
	{
		'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
	},
	{
		'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
	},
]


# Internationalization
# https://docs.djangoproject.com/en/1.10/topics/i18n/

LOCALE_PATHS = (
    os.path.join(BASE_DIR, 'locale'),
)


LANGUAGE_CODE = env.LANGUAGE_CODE



TIME_ZONE = env.TIME_ZONE

USE_I18N = env.USE_I18N

USE_L10N = env.USE_L10N

USE_TZ = env.USE_TZ




# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.10/howto/static-files/

STATIC_URL = env.STATIC_URL
STATIC_ROOT = env.STATIC_ROOT
MEDIA_ROOT = env.MEDIA_ROOT
MEDIA_DOMAIN = env.MEDIA_DOMAIN


# Email settings 
EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
EMAIL_HOST = env.EMAIL_HOST
EMAIL_PORT = env.EMAIL_PORT
EMAIL_USE_TLS = env.EMAIL_USE_TLS
EMAIL_HOST_USER = env.EMAIL_HOST_USER
EMAIL_HOST_PASSWORD = env.EMAIL_HOST_PASSWORD


# FRONTEND PASSWORD RESET 

FRONTEND_PASSWORD_RESET = env.FRONTEND_PASSWORD_RESET


# Cron jobs

CRONJOBS  = [
	# Format example
	#('*/5 * * * *', 'myapp.cron.my_scheduled_job')

	# Expire tokens every 50 minutes
	('*/50 * * * *','common.cron.expireTokens'),

	# Delete date expired tokens hourly
	('*/60 * * * *','common.cron.deleteExpiredTokens'),
]


